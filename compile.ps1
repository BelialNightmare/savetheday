param (
    [switch]$debug = $false,
    [switch]$clean = $false
)

$lcc = "..\gbdk\bin\lcc.exe"
$csources = (Get-ChildItem -Path.\src\ -Recurse -Filter "*.c").FullName
$asmsources = (Get-ChildItem -Path.\src\ -Recurse -Filter "*.asm").FullName
$projectName = "savetheday"
$filename = "$projectName.gb"
[string[]]$filters = @(
    "*.o", "*.lst", "*.map", "*.gb", "*.ihx", "*.sym", "*.cdb", "*.adb", "*.asm", "*.noi"
)

if ($clean) {
    Remove-Item -Path $filters -Force
    exit
}

if ($debug) {
    Invoke-Expression "$lcc -debug -o $filename $csources $asmsources -Iinclude"
}
else {
    Invoke-Expression "$lcc -o $filename $csources $asmsources -Iinclude"
}
