#ifndef __H_SAVETHEDAY_COMMON__
#define __H_SAVETHEDAY_COMMON__

#include <stdint.h>

typedef struct Vector8 {
  int8_t x;
  int8_t y;
} Vector8;

extern const Vector8 directions_for_two_frame_objects[7];
extern uint8_t joypad_current, joypad_previous, two_frame_real_value;

void update_two_frame_counter(void);

#endif /* __H_SAVETHEDAY_COMMON__ */
