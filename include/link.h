#ifndef __H_SAVETHEDAY_LINK__
#define __H_SAVETHEDAY_LINK__

#include <stdint.h>

void game_setup_link(void);
uint8_t game_update_link(void);

#endif // __H_SAVETHEDAY_LINK__
