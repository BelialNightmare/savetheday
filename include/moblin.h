#ifndef __H_SAVETHEDAY_MOBLIN__
#define __H_SAVETHEDAY_MOBLIN__

#include <stdint.h>

void game_setup_moblin(void);
uint8_t game_update_moblin(uint8_t last_sprite);

#endif // __H_SAVETHEDAY_MOBLIN__
