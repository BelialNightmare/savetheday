#include "common.h"

const Vector8 directions_for_two_frame_objects[7] = {
    {0, 1},          // down
    {0, 0}, {0, -1}, // up
    {0, 0}, {1, 0},  // right
    {0, 0}, {-1, 0}  // left
};

uint8_t joypad_current = 0;
uint8_t two_frame_real_value = 0;
uint8_t two_frame_counter = 0;

void update_two_frame_counter(void) {
  two_frame_counter += 3;
  two_frame_real_value = two_frame_counter >> 3;

  // Stop and reset if the value is over 2
  if (two_frame_real_value > 2) {
    two_frame_real_value = 0;
    two_frame_counter = 0;
  }
}
