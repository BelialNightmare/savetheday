#include "link.h"

#include <gb/cgb.h>
#include <gb/gb.h>
#include <gb/metasprites.h>
#include <stdint.h>


#include "common.h"
#include "graphics/LinkDown.h"
#include "graphics/LinkRight.h"
#include "graphics/LinkUp.h"


static const int8_t link_speed = 5;

static uint8_t link_direction = 0;
static uint16_t link_x = 0, link_y = 0;

static uint8_t flip_link = FALSE;

static const metasprite_t *link_metasprite;

void game_setup_link(void) {
  set_sprite_data(0, LinkDown_TILE_COUNT, LinkDown_tiles);

  link_x = 80 << 4;
  link_y = 40 << 4;

  link_direction = J_DOWN;

  link_metasprite = LinkDown_metasprites[0];
}

uint8_t game_update_link(void) {
  uint8_t link_last_direction = link_direction;
  uint8_t link_moving = FALSE;
  link_direction = link_direction;

  if (joypad_current & J_RIGHT) {
    link_x += link_speed;
    link_direction = J_RIGHT;
    link_moving = TRUE;
  }
  if (joypad_current & J_LEFT) {
    link_x -= link_speed;
    link_direction = J_LEFT;
    link_moving = TRUE;
  }

  if (joypad_current & J_DOWN) {
    link_y += link_speed;
    link_direction = J_DOWN;
    link_moving = TRUE;
  }

  if (joypad_current & J_UP) {
    link_y -= link_speed;
    link_direction = J_UP;
    link_moving = TRUE;
  }

  if (link_moving) {
    if (link_direction != link_last_direction) {
      switch (link_direction) {
      case J_RIGHT:
        link_metasprite = LinkRight_metasprites[0];
        break;
      case J_LEFT:
        link_metasprite = LinkRight_metasprites[0];
        break;
      case J_DOWN:
        link_metasprite = LinkDown_metasprites[0];
        break;
      case J_UP:
        link_metasprite = LinkUp_metasprites[0];
        break;
      }
      if (link_direction == J_LEFT) {
        flip_link = TRUE;
      } else {
        flip_link = FALSE;
      }
    }
  }

  if (flip_link) {
    return move_metasprite_vflip(link_metasprite, 0, 0, link_x >> 4,
                                 link_y >> 4);
  } else {
    return move_metasprite(link_metasprite, 0, 0, link_x >> 4, link_y >> 4);
  }
}
