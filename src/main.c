#include <gb/cgb.h>
#include <gb/gb.h>
#include <gb/metasprites.h>
#include <stdint.h>


#include "common.h"
#include "graphics/palettes.h"
#include "link.h"
#include "moblin.h"


void main(void) {
  // Turn on sprites
  // enable the 8x16 tall sprite mode
  SHOW_SPRITES;
  SPRITES_8x16;
  DISPLAY_ON;

  // Set the colour palletes into vram
  set_sprite_palette(0, palettes_PALETTE_COUNT, palettes_palettes);

  game_setup_link();
  game_setup_moblin();

  // Loop forever
  while (1) {
    joypad_current = joypad();

    update_two_frame_counter();

    uint8_t last_sprite = 0;

    last_sprite += game_update_link();
    last_sprite += game_update_moblin(last_sprite);

    // Hide any extra sprites
    hide_sprites_range(last_sprite, 40);

    // Done processing, yield CPU and wait for start of next frame
    wait_vbl_done();
  }
}
