#include "moblin.h"

#include <gb/cgb.h>
#include <gb/gb.h>
#include <gb/metasprites.h>

#include "common.h"
#include "graphics/Moblin.h"

#define MOBLIN_COUNTER_RESET 255u

static const uint8_t MOBLIN_SPEED = 4;
static const uint8_t link_padded_tile_count = 12;
static const uint8_t MOBLIN_COUNTER_WALK_LIMIT = 128u;

static uint8_t moblin_direction = 0;
static uint16_t moblin_x = 0, moblin_y = 0;
static uint16_t moblin_counter = MOBLIN_COUNTER_RESET;

static const metasprite_t *moblin_metasprite = 0;

static const uint8_t directions_only[] = {0, 2, 4, 6};

void game_setup_moblin(void) {
  set_sprite_data(link_padded_tile_count, Moblin_TILE_COUNT, Moblin_tiles);

  moblin_x = 80 << 4;
  moblin_y = 100 << 4;
  moblin_direction = 0;

  moblin_metasprite = Moblin_metasprites[0];
}

uint8_t game_update_moblin(uint8_t last_sprite) {
  if (moblin_counter <= 1) {
    moblin_counter = MOBLIN_COUNTER_RESET;

    moblin_direction = directions_only[DIV_REG % 4];
  } else {
    moblin_counter--;
  }

  if (moblin_counter > MOBLIN_COUNTER_WALK_LIMIT) {
    moblin_x +=
        directions_for_two_frame_objects[moblin_direction].x * MOBLIN_SPEED;
    moblin_y +=
        directions_for_two_frame_objects[moblin_direction].y * MOBLIN_SPEED;

    moblin_metasprite =
        Moblin_metasprites[moblin_direction + two_frame_real_value];
  }

  return move_metasprite(moblin_metasprite, link_padded_tile_count, last_sprite,
                         moblin_x >> 4, moblin_y >> 4);
}
